package com.example.aidlservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.Random;

public class MyService extends Service {
    private int count;

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    class MyBinder extends IMyAidlInterface.Stub {

        IClientCallBack callBack;

        @Override
        public String getName() throws RemoteException {
            count++;
            if (count == 10) {
                ArrayList<String> arrayList = new ArrayList<>();
                for (int i = 0; i < 10; i++) {
                    Random random = new Random();
                    arrayList.add((int) (random.nextInt(100)) + "");
                }
                if (callBack != null) {
                    try {
                        //数据推送到客户端（Activity）
                        callBack.update(arrayList);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }

            }
            return "test" + count;
        }

        @Override
        public User getUser() throws RemoteException {
            return new User("wswf");
        }

        @Override
        public void register(IClientCallBack callback) throws RemoteException {
            this.callBack = callback;
        }
    }
}