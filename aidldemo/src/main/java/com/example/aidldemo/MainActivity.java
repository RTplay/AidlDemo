package com.example.aidldemo;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.example.aidlservice.IClientCallBack;
import com.example.mylibrary.MyLib;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    MyLib mMyLib;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv1 = findViewById(R.id.textview1);
        TextView tv = findViewById(R.id.textview);
        Button bt = findViewById(R.id.button);
        mMyLib = new MyLib(this);
        mMyLib.setOnConnectedListener(() -> mMyLib.registerCallBack(new IClientCallBack.Stub() {
                                    @Override
                                    public void update(List<String> list) {
                                        Log.d("MainActivity", "update: ++++");
                                        tv1.setText("update");
                                    }
                                }
        ));

        bt.setOnClickListener(v -> {
            tv.setText("" + mMyLib.getName());
            Log.d("MainActivity", "onCreate: ++++");
        });
        Button bt1 = findViewById(R.id.button1);
        bt1.setOnClickListener(v -> {
            tv.setText("" + mMyLib.getUser().getName());
        });

        Button bt3 = findViewById(R.id.button3);
        bt3.setOnClickListener(v -> {

        });
    }
}