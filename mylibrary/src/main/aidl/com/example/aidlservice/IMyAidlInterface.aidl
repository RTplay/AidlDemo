// IMyAidlInterface.aidl
package com.example.aidlservice;

// Declare any non-default types here with import statements
import com.example.aidlservice.User;
import com.example.aidlservice.IClientCallBack;
interface IMyAidlInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
//    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
//            double aDouble, String aString);

    String getName();
    User getUser();
    void register(IClientCallBack callback);
}