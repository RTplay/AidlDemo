// IClientCallBack.aidl
package com.example.aidlservice;

// Declare any non-default types here with import statements

interface IClientCallBack {
    void update(in List<String> arrayList);
}